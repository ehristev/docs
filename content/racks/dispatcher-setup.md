---
title: Dispatcher setup
---

# [Cyberserver Xeon E-RS100-E100](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 

## New dispatcher

Dispatcher can be set up as per [these guidelines](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-dispatcher.md).

Once you have the mac addresses for the ethernet and the IPMI, open a sysadmin
support request highlighting the need for static IP's on the router in the lava
DMZ. Provide the prefered name of the device along with the identified mac addresses
for the IPMI interface and the network for the dispatcher. These can be added at any
time before the dispatcher goes online 

## Replacement dispatcher

Replacement dispatcher can be installed the same as a new dispatcher up to 
the chef setup section where the following amendments neeed to be made:

Create a [sysadmin support request](https://phabricator.collabora.com/maniphest/task/edit/form/3/) 
asking for the mac addresses to be updated on bakora for the ipmi interface and 
the network interface. This needs to be coordinated with the maintenance window 
to change the dispatchers over as the current one needs to remain until then. 
This will be updated on the officeserver chef cookbook.

We can then set up the server to be able to be able to use the chef configrations
for the current dispatcher. First we need to add the collabora software repos:

Add the collabora PGP key...

```
apt -y install gnupg

cat > collabora.key <<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.9 (GNU/Linux)

mQENBEoVZq8BCACg7h6N6dV52UNayJfboK1ZvlaaEGYjQJa48siGnORmUAl6B/93
F1R6e/8VRxMbFW7G06UqFAsjNdEugBwTEZKQbUjiJpcV7BQYFX+VyYK3A+FOwWHj
xCedBMblBL8uDJ17Fet8d03VcfDcARNpsZm6EzhjHgvtT5AZKgUPqko6dslu7FIq
rVrEF8csqAy4dm9Yzt0oOl5T7dSUOr41vNCOwmPzcPHxeNu7+MBx6CBtxxL7d6GV
jM3fRVISrbz6XcDnoVnykKV+3h346jBveyqoqChB+/txP/SS7dLik3fxNa6dRWO1
6lo5kaftiBVCCggZLk0jCI5YcOI/nE7f1tAzABEBAAG0OUNvbGxhYm9yYSBzeXNh
ZG1pbiBhcmNoaXZlIGtleSA8c3lzYWRtaW5AY29sbGFib3JhLmNvLnVrPokBNgQT
AQIAIAUCShVmrwIbAwYLCQgHAwIEFQIIAwQWAgMBAh4BAheAAAoJEE2GwomUBGP/
hS8H/jmG9VwpxfSWGn10UHPW5z6kKn6TKkAmkFuGRWdabRe6f2b3XGmk9ZnwQQii
eJyqSpQmFvL9pCOG3UPf2icqbuzhrcIb5jFUcnTTCV9n0HrRsUqtzv86tFBVQAe6
60rH29wSiavc5K2idH+6feoru+BLM6UAdaIuQ6aD7WIga6Skb4lmQX3n73OgzUAW
DqYesUQg9W4dZb5/1WBh+993g2FboMZ6E7yfa0gyl7dYxj+DA0HRwDDxQ0hZ/1+w
J3w2wZ3eO7KSrURiXxyMdN7fLkx72frjMdqJw4JC4skeJkv9rKGXDNYSaAjFpl1n
IqoftyWscYtHez7Mt6hGRGMoK+g=
=pv+z
-----END PGP PUBLIC KEY BLOCK-----
EOF

gpg --no-default-keyring --keyring ./temp-keyring.gpg --import collabora.key

gpg --no-default-keyring --keyring ./temp-keyring.gpg --export > ./collabora-archive-keyring.gpg

cp ./collabora-archive-keyring.gpg /usr/share/keyrings/

```

Then add the repos...

```

apt -y install apt-transport-https

cat > /etc/apt/sources.list.d/collabora-sysadmin-tools.list <<EOF
deb [signed-by=/usr/share/keyrings/collabora-archive-keyring.gpg] https://repositories.collabora.co.uk/sysadmin/debian buster tools
deb-src [signed-by=/usr/share/keyrings/collabora-archive-keyring.gpg] https://repositories.collabora.co.uk/sysadmin/debian buster tools
EOF

apt update

```

Finally we can install the packages along with some other essentials...

```
apt -y install chef rkhunter ca-certificates eatmydata lsb-release
```

At this point, a Debconf prompt will ask for a Chef server URL. Provide https://chef.collabora.co.uk/, and continue. The last thing we need to add is the chef validation certificate...

```
cat > /etc/chef/validation.pem <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAna9vBuUpup9TA29rciasGqRfJ2rmmsDjNuiUqKjff2d6XFnW
XocYHaBFIqxGeLzCokjhvSRyyTyBtAp+lHK3zbzLNs5Cfki9JOQvGrJqViA+2gbD
DgdvjzUPYENh4wINo3zdxCpMKXR/q1UCtwjdEFYC5tFAbBAM0Rfdxl0geVkxBgih
Q6ChSbR76/QlAFer7PYKyczWB/eLlLOAPWyUfAYHr/2vbg+uyY+/WhBmh/5cTRtw
lKxfQuKYAI0xuDYXRU7Zhp6zvzeHG+30INgFzbb821O8gFPurwQdkFTcvJ2yGLuf
FKJdYr0VaGD8XVMs7AAZaBWhoQ2di7zkOMoxEwIDAQABAoIBAGu89BU3+p0CQwvz
0uv5tFm3uTQLfGh8JkLcAsaCK8K3BwmWcDvB/h4v5UzdfID+J9YOCf7bqnoIl25W
RUpUX3V46zgVEaWLOYC9T9nj0XfrLEZ3nEo3lme4jzrsjzM1se/kXvmrFMK2UIzV
1rcMnU6OFglzK5sqFDfBIMxp0MhXr+UhIHa3dmJ6v6XP5nA1Q+i10/yPaz3OUBYE
WjllbzfvSbvsNtmYXR3WyiiEUlSgESk9zV4PyKlXsY5CEPYrFSV9YeIn5L003abe
lQagmdC2rkkH6PonvHmcAq6IabU8dzdnHKvJl7JtnDTiqfa45HNL7GcFR9qk6yv+
AzL3q5ECgYEAzPIxUjn6KDjfNWicUf1jrlkb+0NlNXIjT/fhBZwt0KvZc+3iQEFU
e3O20DmbLGp/T6tvJWo0C18vNUOCJCtJd7Kd2IO/c4FGPmYYG1PyrzuQvxCBdvOd
OpNiiOWu+vHCnPeG/Rl5zpTEzWyS76eZWoXxuk4F9CTxEdVhaNrvX78CgYEAxPdU
bIcA8gFm45qo8slZbximYrTdg89SmqbTGFfRGJntbZWAsOZHPgxKtEpZm82x4CI1
7GZvjbSokFaYU6bywYd+jGh0ZLME+AJiyQaVXWyEQkyKhakoabWc0ouZEBpVdcWv
Oss5jU57ja7/GPKtNvYUOj54MUxtMS7DlVbow60CgYEAi25+xa1yFulSWni4WUOm
DJjNY62fbrQfeClxayqtNNml0KxYEFUiD/dSuelnO9mckexVasQNcmsop/ks0kSs
8AWSRBqmJwQdcRhucDE126gPux2UpwPoxVA+alqxKENQnw/8Q7eQWk52cjfa47dH
qwGTyqeIf2iepf5hDkBdOOsCgYBw4i8IY/cuu470MtdRDgSrnzz8pRuvR0rYR3QE
J5A2wUFJ5Lz0pYV/EhxeZ54jpSekhPbSnYKhfEB/OKHkj3HhaH4milbqv47BdNyY
yaUg/ehu9t8M632119gg8tCuYelIrhtXXtGFIs28zeZ1rd+H+FBvcQ4BcwPBhEOj
e0O7oQKBgDuxbmaykPj6t3dhxnhYMNOgrXVWNLVvHOjHUv+yuxJt0ziVI6ky3buM
T2DEphCi+mrwBG8bAui2tyyHp+2c/4hPQm2OjNNWn4XU71cZG/35GyuFPe/3dsxr
/dmvxdSn4QMyfrPfkRzAwQINN96OSTmw6SRxbcEwmNIiv+ZYNhA7
-----END RSA PRIVATE KEY-----
EOF
```

The dispatcher has 64GB Ram and we are allocating 60GB of the ram to 
the temporary directory that lava uses for the downloaded files.
This can be set up but not processed until lava has been installed or 
the location created manually. Please add these lines to /etc/fstab:

```
# tmpfs for temporary Lava directory
#tmpfs /var/lib/lava/dispatcher/tmp/ tmpfs defaults,size=60G 0 0
```

And the server should be ready for the maintenance window.


Check to see if the network device name differs between the old and new dispatchers.
If they are different then the chef node entry for the dispatcher will need to be updated, 
using the command `knife node edit <dispatcher name>`,
when the current dispatcher has been turned off during the maintenance window, 
for example:

```
...
  "virbr": {
    "domain": "lava-rack",
    "raw-device": "eno1"
  },
  "vlan": {
    "domain": "lava-rack",
    "raw-device": "eno1"
  }
...
```
  --TO--
```
...
  "virbr": {
    "domain": "lava-rack",
    "raw-device": "enp1s0"
  },
  "vlan": {
    "domain": "lava-rack",
    "raw-device": "enp1s0"
  }
...
```

As a guide, for during the maintenance window, your action plan might look something like the following:


* Maintenance dispatcher in lava.
* Wait for jobs to complete.
* Turn off old dispatcher.
* Update bakora with new mac addresses. (Sysadmin)
* Update node entry (network name as above ) for lava-rack-cbg-X in chef with 'knife node edit lava-rack-cbg-X....
* Hook up new dispatcher to power/switch (not usbhub yet)
* Turn on new dispatcher.
* Process first chef scripts
* Run chef-client and fix any issues, run until the configuration is there for the devices
* Add the tmpfs filesystem to fstab for /var/lib/lava/dispatcher/tmp
* Ensure the vlan2 network interface has been raised.
* Plug in usb hub, check for couple of consoles.
* Reboot
* Check console again
* Bring out of maintenance in lava

You may need to manually restart the dispatcher during the chef processes as the networking is reset 

There may be an issue with authorization between the chef client and the chef server. 
This can be overcome by updating the chef certificate on the dispatcher. On your computer
run `knife client reregister <dispatcher name>` and update the certificate on the dispatcher in `/etc/chef/client.pem`.

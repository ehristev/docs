---
title: lava-rack-cbg-B
---

# [lavandula](https://lava.collabora.co.uk/scheduler/worker/lavandula) Dispatcher and controller

Dispatcher is [Cyberserve Xeon E-RS100-E10](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 
and the server specification can be found on one of [the quotations from broadberry.](/files/PDAR52995.pdf) 
The server can be accessed over SSH and has the IP address of 10.154.97.149. 
The switch for the rack is a [Zyxel GS1900-24](https://www.zyxel.com/uk/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/specification) 
and is on IP address 10.154.97.249. 
The USB hub is a [Manhatten MondoHub II](https://manhattanproducts.eu/products/manhattan-en-mondohub-ii-163606).


# Devices from top to bottom

[pms-0](https://www.aviosys.com/products/9850.php) 
Power to the vertical power bar, network connected via ethernet cable to the switch. 
Configuration for the IP address is in the chef repository, 
provides power switching for the devices on the same shelf which are:  
[meson-g12b-a311d-khadas-vim3-cbg-3](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-3) 
Power connected to pms-0, 
uart console connected to USB hub via [USB to UART 3 way female pin headers cable](https://ftdichip.com/products/ttl-232r-rpi/), 
network connected via ethernet cable to the switch.  
[meson-g12b-a311d-khadas-vim3-cbg-4](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-4) 
Power connected pms-0, 
uart console connected to USB hub via 'USB to UART 3 way female pin headers cable', 
network connected via ethernet cable to the switch.  
[meson-g12b-a311d-khadas-vim3-cbg-5](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-5) 
Power connected pms-0, 
uart console connected to USB hub via 'USB to UART 3 way female pin headers cable', 
network connected via ethernet cable to the switch.  
[meson-g12b-a311d-khadas-vim3-cbg-6](https://lava.collabora.co.uk/scheduler/device/meson-g12b-a311d-khadas-vim3-cbg-6) 
Power connected pms-0, 
uart console connected to USB hub via 'USB to UART 3 way female pin headers cable', 
network connected via ethernet cable to the switch.  

[5v PSU](https://uk.farnell.com/tracopower/txm-075-105/power-supply-ac-dc-75w-5v-12a/dp/2363868?ost=txm+075-105) 
Power connected to horizontal power bar.  
[12v PSU](https://uk.farnell.com/tracopower/txm-075-112/power-supply-ac-dc-75w-12v-6a/dp/2363869) 
Power connected to horizontal power bar.

[eth8020-0](https://www.robot-electronics.co.uk/eth8020-20-x-16a-ethernet-relay.html) 
Power is to the 12v PSU, 
network connected via ethernet cable to the switch. 
Configuration is in the chef repository. provides power switching for devices on this and the next self.  
[imx6q-sabrelite-lava-cbg-6](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-6) 
Power is connected to '5v PSU' via 'eth8020-0' port 1, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[imx6q-sabrelite-lava-cbg-7](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-7) 
Power is connected to '5v PSU' via 'eth8020-0' port 2, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[imx6q-sabrelite-lava-cbg-8](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-8) 
Power is connected to '5v PSU' via 'eth8020-0' port 3, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  

[rk3399-rock-pi-4b-cbg-2](https://lava.collabora.co.uk/scheduler/device/rk3399-rock-pi-4b-cbg-2) 
Power is connected to '5v PSU' via 'eth8020-0' port 4, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected vai ethernet cable to the switch.  
[rk3399-rock-pi-4b-cbg-3](https://lava.collabora.co.uk/scheduler/device/rk3399-rock-pi-4b-cbg-3) 
Power is connected to '5v PSU' via 'eth8020-0' port 5, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected vai ethernet cable to the switch.  
[bcm2711-rpi-4-b-cbg-2](https://lava.collabora.co.uk/scheduler/device/bcm2711-rpi-4-b-cbg-2) 
Power is connected to '12v PSU' via 'eth8020-0' port 8 
and a [nid35 voltage regulator](https://www.meanwell-web.com/content/files/pdfs/productPdfs/MW/NID35/NID35-spec.pdf), 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[bcm2711-rpi-4-b-cbg-3](https://lava.collabora.co.uk/scheduler/device/bcm2711-rpi-4-b-cbg-3) 
Power is connected to '12v PSU' via 'eth8020-0' port 8 and a 'nid35 voltage regulator', 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[minnowboard-turbot-E3826-cbg-3](https://lava.collabora.co.uk/scheduler/device/minnowboard-turbot-E3826-cbg-3) 
Power is connected to '12v PSU' via 'eth8020-0' port 10 and a 'nid35 voltage regulator', 
console is connected to the USB hub using a [USB to UART 6pin SIL](https://ftdichip.com/products/ttl-232r-3v3/), 
network is connected via ethernet cable to the switch.

USB hub connected via hard wired usb to the Disptcer Type-A and to the vertical power bar, 
then to each of the devices.
Server connected to the [horizontal power bar](https://www.racksolutions.co.uk/catalog/product/view/id/2722/s/8-way-13-amp-horizontal-rack-mount-power-strips/), 
USB connection to the USB hub and network connection to the switch. 
Configured via [chef](https://gitlab.collabora.com/lava/collabora-lava-setup)

Switch Power connected to the horizontal power bar, connection to the core switch, 
connection to the dispatcher and then to each of the devices. 
Configuration found [here](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-assembly.md#managed-ethernet-switch)

Space.

Space.


# [marula](https://lava.collabora.co.uk/scheduler/worker/marula) Dispatcher and controller

Dispatcher is [Cyberserve Xeon E-RS100-E10](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 
and the server specification can be found on one of [the quotations from broadberry.](/files/PDAR52995.pdf) 
The server can be accessed over SSH and has the IP address of 10.154.97.150. 
The switch for the rack is a [Zyxel GS1900-24](https://www.zyxel.com/uk/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/specification) 
and is on IP address 10.154.97.250. 
The USB hub is a 7 port hub powered from the 12v power supply (to be replaced).

# Devices from top to bottom

USB hub connected to the 12v power supply and the devices

Server connected to the 'Horizontal power bar', 
USB connection to the USB hub and network connection to the switch. 
Configured via [chef](https://gitlab.collabora.com/lava/collabora-lava-setup)

Switch Power connected to the horizontal power bar, connection to the core switch, 
connection to the dispatcher and then to each of the devices. 
Configuration found [here](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-assembly.md#managed-ethernet-switch)

[5v PSU](https://uk.farnell.com/tracopower/txm-075-105/power-supply-ac-dc-75w-5v-12a/dp/2363868?ost=txm+075-105) 
Power connected to horizontal power bar.  
[12v PSU](https://uk.farnell.com/tracopower/txm-075-112/power-supply-ac-dc-75w-12v-6a/dp/2363869) 
Power connected to horizontal power bar.

[eth8020-0](https://www.robot-electronics.co.uk/eth8020-20-x-16a-ethernet-relay.html) 
Power is to the 12v PSU, network connected via ethernet cable to the switch. 
Configuration is in the chef repository. 
provides power switching for devices on this and the next self.  
[imx6q-sabrelite-lava-cbg-3](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-3) 
Power is connected to '5v PSU' via 'eth8020-0' port 1, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[imx6q-sabrelite-lava-cbg-4](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-4) 
Power is connected to '5v PSU' via 'eth8020-0' port 2, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[imx6q-sabrelite-lava-cbg-5](https://lava.collabora.co.uk/scheduler/device/imx6q-sabrelite-lava-cbg-5) 
Power is connected to '5v PSU' via 'eth8020-0' port 3, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  

[rk3399-rock-pi-4b-cbg-0](https://lava.collabora.co.uk/scheduler/device/rk3399-rock-pi-4b-cbg-0) 
Power is connected to '5v PSU' via 'eth8020-0' port 4, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected vai ethernet cable to the switch.  
[rk3399-rock-pi-4b-cbg-1](https://lava.collabora.co.uk/scheduler/device/rk3399-rock-pi-4b-cbg-1) 
Power is connected to '5v PSU' via 'eth8020-0' port 5, 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected vai ethernet cable to the switch.  
[bcm2711-rpi-4-b-cbg-0](https://lava.collabora.co.uk/scheduler/device/bcm2711-rpi-4-b-cbg-0) 
Power is connected to '12v PSU' via 'eth8020-0' port 6 and a 'nid35 voltage regulator', 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[bcm2711-rpi-4-b-cbg-1](https://lava.collabora.co.uk/scheduler/device/bcm2711-rpi-4-b-cbg-1) 
Power is connected to '12v PSU' via 'eth8020-0' port 7 and a 'nid35 voltage regulator', 
console is connected to the USB hub using 'USB to UART 3 way female pin headers cable', 
network is connected via ethernet cable to the switch.  
[minnowboard-turbot-E3826-cbg-2](https://lava.collabora.co.uk/scheduler/device/minnowboard-turbot-E3826-cbg-2) 
Power is connected to '5v PSU' via 'eth8020-0' port 8, 
console is connected to the USB hub using a 'USB to UART 6pin SIL', 
network is connected via ethernet cable to the switch.

Space.

Space.

Space.

Space.

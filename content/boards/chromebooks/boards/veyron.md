---
title: Veyron Chromebooks
---

`veyron` is a board name for ARM Chromebooks based on the Rockchip RK3288 SoC.

The Collabora LAVA lab contains the following `veyron` devices:

-   Medion Chromebook S2013 (codename `jaq`)
    -   See [rk3288-veyron-jaq](https://lava.collabora.dev/scheduler/device_type/rk3288-veyron-jaq) in LAVA
    -   CPU: 4x 1.8 GHz Cortex-A17
    -   GPU: Mali-T760 MP4
    -   SoC: Rockchip RK3288

Full SoC specs: <http://rockchip.wikidot.com/rk3288>

Mainline Linux kernel support was added in release v4.4-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm/boot/dts/rk3288-veyron-jaq.dts?h=v4.4-rc1>

### Debugging interfaces

`veyron` boards have been flashed and tested with [ServoV2](../../01-debugging_interfaces).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    earlyprintk=ttyS2,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp tftpserverip=10.154.97.199 ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

The IP and path of the NFS share are examples and should be adapted to
the test setup.

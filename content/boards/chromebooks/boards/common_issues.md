---
title: Common issues
---

This is a list of the issues we found that were common for all the
Chromebooks tested in the lab.


## Issues related to Servo v4

The Servo v4 debugging interface causes different issues depending on
the Chromebook board and the lab setup.

### Network interface issues

#### R8152 driver not reliable at Gigabit speeds

The Realtek R8152 USB-Eth adapter can be found in the Servo v4 and in
other standalone dongles.

The R8152 Ethernet driver in Depthcharge is not reliable when working at
Gigabit speeds. It's recommeded to configure the link as Fast Ethernet
(100 Mbps) when booting over TFTP.

```
sudo ethtool -s <eth_interface> speed 100 duplex full autoneg off
```


#### Ethernet frame reception issues

Ethernet frame reception may fail and hang Depthcharge when using the
Servo v4 as an USB-Eth adapter (error: `R8152: Bulk read error`). Bigger
file transfers make this more likely to happen, although it's not
completely deterministic.


### Stability issues

#### Debugging interface unstable when using Servo v4 connected to power supply

Observed in:

- `coral`: R64-10068.111.0.serial
- `rammus`: R72-11275.110.0.serial
- `octopus`: R72-1197.144.0.serial

with Servo v4 FW 2.3.22 and 2.4.35.

When the Servo v4 is connected to a power supply the debugging
interface becomes unstable. Some of the effects seen:

  - Charging is unreliable.
  - Serial consoles stop working. The following message appears in the
    kernel log:

```
google ttyUSB4: usb_serial_generic_write_bulk_callback - urb stopped: -32
```
  - Ethernet data transfer through the Servo fails.
  - Cr50 commands lost sometimes.

Basically, all communication going through the Servo starts failing
intermittently.

## Hardware issues

### Spurious events causing suspend

There appears to be some spurious events such as a power button press or low
battery that may result in the device to suspend.  When using Debian, systemd
typically listens for such events and initiates a suspend-to-RAM when they
occur.  The root cause of these events is not yet known, so a workaround has
been put in place to disable suspend with Debian NFS root images:

```
[Sleep]
AllowSuspend=no
AllowHibernation=no
AllowSuspendThenHibernate=no
AllowHybridSleep=no
```

See commit on Gitlab: [4be260d4  Add nosuspend overlay ](https://gitlab.collabora.com/lava/health-check-images/-/commit/4be260d463f291a1b85d8a1637d1ba9003742d46)

The same workaround has been applied also to the Debian NFS rootfs images used by KernelCI: [c6f377793090 config/rootfs/debos: add nosuspend
overlay](https://github.com/kernelci/kernelci-core/commit/c6f3777930907e6b8525777624e8d4385047fefb)

### Serial console drops

The AP serial console occasionally gets stuck at random points during the boot and test phases (console becomes unresponsive and output stops flowing); this issue has been observed on several device types across different racks, both with ServoV4/ServoV4.1 and SuzyQable. Normally closing and re-opening the terminal shows that the Chromebooks are still working and output starts flowing again, but currently there's no automated mechanism to detect these events in LAVA and act accordingly. The root cause of this issue is not yet known.

Some workarounds have been put in place by both KernelCI and MesaCI. For KernelCI tast tests, the LAVA job is configured to run all tests from inside a docker container, connecting to the DUT over SSH.

MesaCI's Gitlab runner in turn monitors the job output and cancels the jobs whenever the console becomes unresponsive for more than a fixed time interval.
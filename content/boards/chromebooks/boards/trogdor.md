---
title: Trogdor Chromebooks
---

`trogdor` is a board name for ARM-64 Chromebooks using an Qualcomm Snapdragon 7c.

The Collabora LAVA lab contains the following `trogdor` devices:

-   [Acer Chromebook Spin 513](https://www.acer.com/ac/en/US/content/series/acerchromebookspin513) (codename `lazor`)
    -   Label: `LIMOZEEN, PVT, US, SKU3, QSIP-7180, 4GB RAM, 32GB EMMC, 11.6IN, HD, WIFI, BT, LTE`
    -   See [sc7180-trogdor-lazor-limozeen](https://lava.collabora.dev/scheduler/device_type/sc7180-trogdor-lazor-limozeen) in LAVA
    -   CPU: Qualcomm Kryo 468
    -   GPU: Qualcomm Adreno 618 GPU
    -   SoC: Qualcomm Snapdragon 7c
    -   Arch: Aarch64

Full SoC specs: <https://www.qualcomm.com/products/application/mobile-computing/snapdragon-8-series-mobile-compute-platforms/snapdragon-7c-compute-platform>

Mainline Linux kernel support was added in release v5.17-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/qcom/sc7180-trogdor-lazor-limozeen-nots-r5.dts?h=v5.17-rc1>

### Debugging interfaces

`Trogdor` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The  supports CCD and can be debugged through
its USB-C port on the left side (power supply's one).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through the Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

-   Serial is very racy, often causing interference between kernel messages and LAVA signals (even when redirected to `/dev/kmsg`). The root cause of this issue is not yet known.
-   The driver for the onboard USB controller, when compiled as a module, is loaded too late in the boot process, preventing the IP from being assigned in time for mounting the NFS.

See also [Common issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

    earlyprintk=ttyMSM0,115200n8 console=ttyMSM0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `lazor` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/trogdor-lazor.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

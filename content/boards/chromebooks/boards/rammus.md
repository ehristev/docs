---
title: Rammus Chromebooks
---

`rammus` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

-   [ASUS Chromebook Flip
    C433](https://www.asus.com/Laptops/For-Home/Chromebook/ASUS-Chromebook-Flip-C433/)
-   [ASUS Chromebook C425](https://www.asus.com/Laptops/For-Students/Chromebook/ASUS-Chromebook-C425).

These chromebooks use 64 bit Intel Amber Lake Y processors like the Core
i5-8200Y, the Core m3-8100Y and the Core i7-8500Y.

The Collabora LAVA lab contains the following `rammus` devices:

-   [ASUS Chromebook Flip C434](https://www.asus.com/it/laptops/for-home/chromebook/asus-chromebook-flip-c434/techspec/)
    (codename `shyvana`)
    -   See [asus-C433TA-AJ0005-rammus](https://lava.collabora.dev/scheduler/device_type/asus-C433TA-AJ0005-rammus) in LAVA
    -   CPU: Intel Core m3-8100Y CPU @ 1.10GHz
    -   GPU: UHD Graphics 615
    -   Arch: x86_64

### Debugging interfaces

`rammus` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the Asus C433, the debug port is the USB-C port in the right side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See also [Common issues](../common_issues.md).

### Example kernel command line arguments

    earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS0,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

the IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `rammus` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/rammus.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `asus-C433TA-AJ0005-rammus`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         2GiB LPDDR3 Synchronous 1867 MHz (0.5 ns)
    /0/1/1                 memory         2GiB LPDDR3 Synchronous 1867 MHz (0.5 ns)
    /0/2                   processor      Intel(R) Core(TM) m3-8100Y CPU @ 1.10GHz
    /0/100                 bridge         Xeon E3-1200 v6/7th Gen Core Processor Hos
    /0/100/2               display        UHD Graphics 615
    /0/100/1c              bridge         Sunrise Point-LP PCI Express Root Port #1
    /0/100/1c/0            network        Wireless 7265
    /0/100/1f              bridge         Intel Corporation
    /0/100/1f.2            memory         Memory controller
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Xeon E3-1200 v6/7th Gen Core Processor Host Bridge/DRAM Registers (rev 02)
    00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 615 (rev 02)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 02)
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)
    00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)
    00:15.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #0 (rev 21)
    00:15.1 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #1 (rev 21)
    00:19.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO UART Controller #2 (rev 21)
    00:19.1 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #5 (rev 21)
    00:1c.0 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #1 (rev f1)
    00:1e.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO UART Controller #0 (rev 21)
    00:1e.2 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO SPI Controller #0 (rev 21)
    00:1e.4 SD Host controller: Intel Corporation Device 9d2b (rev 21)
    00:1e.6 SD Host controller: Intel Corporation Sunrise Point-LP Secure Digital IO Controller (rev 21)
    00:1f.0 ISA bridge: Intel Corporation Device 9d4b (rev 21)
    00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)
    00:1f.3 Multimedia audio controller: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
    00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)
    00:1f.5 Non-VGA unclassified device: Intel Corporation Device 9d24 (rev 21)
    01:00.0 Network controller: Intel Corporation Wireless 7265 (rev 59)
    ```

---
title: Kevin Chromebooks
---

`kevin` is a board name for arm64 Chromebooks based on the Rockchip RK3399 SoC.

The Collabora LAVA lab contains the following `kevin` devices:

-   Samsung Chromebook Plus (codename `kevin`)
    -   See [rk3399-gru-kevin](https://lava.collabora.dev/scheduler/device_type/rk3399-gru-kevin) in LAVA
        -   CPU: 2x ARM Cortex-A72 MPCore, 4x ARM Cortex-A53 MPCore
        -   GPU: ARM Mali-T860MP4
        -   SoC: Rockchip RK3399
        -   Arch: Aarch64

Full SoC specs: <http://rockchip.wikidot.com/rk3399>

Mainline Linux kernel support was added in release v4.12-rc1:
<https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/rockchip/rk3399-gru-kevin.dts?h=v4.12-rc1>

### Debugging interfaces

`kevin` boards have been flashed and tested with [ServoMicro](../../01-debugging_interfaces).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through either the Techrise USB-Eth adapter (R8152-based) or the Asix AX88772B AX88772 USB-Eth adapter.

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS2,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `kevin` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/kevin.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

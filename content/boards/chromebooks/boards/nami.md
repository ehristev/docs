---
title: Nami Chromebooks
---

`nami` is a board name for x86_64-based Chromebooks.

The Collabora LAVA lab contains the following `nami` devices:

-   [HP Chromebook x360 14 G1](https://support.hp.com/si-en/document/c06224432) (codename `sona`)
    -   Label: `G7 PVT SKU4 TLA Notebook`
    -   See [hp-x360-14-G1-sona](https://lava.collabora.dev/scheduler/device_type/hp-x360-14-G1-sona) in LAVA
    -   CPU: Intel Core i7-8650U CPU @ 1.90GHz
    -   GPU: UHD Graphics 620
    -   Arch: x86_64

### Debugging interfaces

`nami` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `nami` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/sona.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `hp-x360-14-G1-sona`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/1                   memory         16GiB System memory
    /0/1/0                 memory         8GiB DDR4 Synchronous 2400 MHz (0.4 ns)
    /0/1/1                 memory         8GiB DDR4 Synchronous 2400 MHz (0.4 ns)
    /0/2                   processor      Intel(R) Core(TM) i7-8650U CPU @ 1.90GHz
    /0/100                 bridge         Xeon E3-1200 v6/7th Gen Core Processor Hos
    /0/100/2               display        UHD Graphics 620
    /0/100/1c              bridge         Sunrise Point-LP PCI Express Root Port #4
    /0/100/1c/0            network        Wireless 7265
    /0/100/1f              bridge         Sunrise Point LPC Controller/eSPI Controll
    /0/100/1f.2            memory         Memory controller
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Xeon E3-1200 v6/7th Gen Core Processor Host Bridge/DRAM Registers (rev 08)
    00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 620 (rev 07)
    00:04.0 Signal processing controller: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Thermal Subsystem (rev 08)
    00:08.0 System peripheral: Intel Corporation Xeon E3-1200 v5/v6 / E3-1500 v5 / 6th/7th/8th Gen Core Processor Gaussian Mixture Model
    00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)
    00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)
    00:15.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #0 (rev 21)
    00:15.1 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #1 (rev 21)
    00:15.2 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #2 (rev 21)
    00:15.3 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #3 (rev 21)
    00:19.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO UART Controller #2 (rev 21)
    00:1c.0 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #4 (rev f1)
    00:1e.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO UART Controller #0 (rev 21)
    00:1e.2 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO SPI Controller #0 (rev 21)
    00:1e.3 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO SPI Controller #1 (rev 21)
    00:1e.4 SD Host controller: Intel Corporation Device 9d2b (rev 21)
    00:1f.0 ISA bridge: Intel Corporation Sunrise Point LPC Controller/eSPI Controller (rev 21)
    00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)
    00:1f.3 Multimedia audio controller: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
    00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)
    00:1f.5 Non-VGA unclassified device: Intel Corporation Device 9d24 (rev 21)
    01:00.0 Network controller: Intel Corporation Wireless 7265 (rev 59)
    ```

---
title: Asurada Chromebooks
---

`asurada` is a board name for arm64 Chromebooks based on the Mediatek MT8192 SoC.

The Collabora LAVA lab contains the following `asurada` devices:

-   [Acer Chromebook 514 CB514-2HT](https://www.acer.com/us-en/chromebooks/acer-chromebook-514-cb514-2h-cb514-2ht/pdp/NX.AS2AA.004) (codename `spherion`)
    -   Label: `CHROMEBOOK, SPHERION DVT, US, SKU4, MEDIATEK MT8192, 8GB RAM, 128GB, TOUCH, 14IN, FHD, WIFI, BT, USED`
    -   See [mt8192-asurada-spherion-r0](https://lava.collabora.dev/scheduler/device_type/mt8192-asurada-spherion-r0) in LAVA
    -   CPU: 4x Arm Cortex-A76 cores operating up to 2.2 GHz, 4x Arm Cortex-A55 cores
    -   GPU: ARM Mali-G57 MC5
    -   SoC: Mediatek Kompanio 820 (MT8192)
    -   Arch: Aarch64

Full SoC specs: <https://www.mediatek.com/products/chromebooks/mediatek-kompanio-820>

Mainline Linux kernel support was added in release v6.0-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/mediatek/mt8192-asurada-spherion-r0.dts?h=v6.0-rc1>

### Debugging interfaces

`asurada` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In the Acer Chromebook 514, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `asurada` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/asurada.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

---
title: Jacuzzi Chromebooks
---

`jacuzzi` is a board name for arm64 Chromebooks based on the Mediatek
MT8183 SoC. It's an evolution of `kukui`.

The Collabora LAVA lab contains the following `jacuzzi` devices:

-   [Acer Chromebook Spin 311 (CP311-3H)](https://www.acer.com/us-en/chromebooks/acer-chromebook-spin-311-cp311-3h) (codename `juniper`)
    -   See [mt8183-kukui-jacuzzi-juniper-sku16](https://lava.collabora.dev/scheduler/device_type/mt8183-kukui-jacuzzi-juniper-sku16) in LAVA
    -   CPU: 4x Arm Cortex-A73, 4x Arm Cortex-A53 cores all operating up to 2GHz
    -   GPU: Arm Mali-G72
    -   SoC: MediaTek Kompanio 500 (MT8183)
    -   Arch: Aarch64

Full SoC specs: <https://www.mediatek.com/products/chromebooks/mediatek-kompanio-500>

Mainline Linux kernel support was added in release v5.13-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/arm64/boot/dts/mediatek/mt8183-kukui-jacuzzi-juniper-sku16.dts?h=v5.13-rc1>

### Debugging interfaces

`jacuzzi` boards have been flashed and tested with [Servo
v4](../../01-debugging_interfaces) interfaces.

The Acer Chromebook Spin 311 supports CCD and can be debugged through
its only USB-C port.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

No specific issues found for this Chromebook yet, but see [Common
issues](../common_issues.md).

### Example kernel command line arguments

An example of kernel command line arguments to boot a FIT image:

    earlyprintk=ttyS0,115200n8 console=tty1 console_msg_format=syslog console=ttyS0,115200n8 root=/dev/ram0 ip=dhcp tftpserverip=<server_ip>

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `juniper` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/jacuzzi.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

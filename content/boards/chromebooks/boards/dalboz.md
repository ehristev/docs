---
title: Dalboz Chromebooks
---

`dalboz` is a board name for x86_64-based Chromebooks and it is a derivative of `zork`.

The Collabora LAVA lab contains the following `dalboz` devices:

-   [ASUS Chromebook CM1400CXA](https://www.asus.com/laptops/for-home/chromebook/asus-chromebook-cx1-cx1400/) (codename `jelboz`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, JELBOZ, PVT, US, SKU1, AMD 3015Ce, 8GB RAM, 32GB, 14IN, FHD, WIFI, BT, USED`
    -   See [asus-CM1400CXA-dalboz](https://lava.collabora.dev/scheduler/device_type/asus-CM1400CXA-dalboz) in LAVA
    -   CPU: AMD 3015Ce
    -   GPU: AMD Picasso
    -   Arch: x86_64

### Debugging interfaces

`dalboz` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In an ASUS Chromebook CM1400CXA Chromebook, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `shuboz` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/dalboz.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `asus-CM1400CXA-dalboz`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path            Device  Class          Description
    ======================================================
    /0/0                        memory         1MiB BIOS
    /0/4                        processor      AMD 3015Ce with Radeon Graphics
    /0/4/6                      memory         64KiB L1 cache
    /0/4/7                      memory         512KiB L2 cache
    /0/4/8                      memory         4MiB L3 cache
    /0/5                        memory         32KiB L1 cache
    /0/a                        memory         8GiB System Memory
    /0/a/0                      memory         8GiB SODIMM DDR4 Synchronous 3200 MHz
    /0/100                      bridge         Raven/Raven2 Root Complex
    /0/100/1.2                  bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/1.2/0                network        RTL8822CE 802.11ac PCIe Wireless Netw
    /0/100/1.3                  bridge         Raven/Raven2 PCIe GPP Bridge [6:0]
    /0/100/8.1                  bridge         Raven/Raven2 Internal PCIe GPP Bridge
    /0/100/8.1/0                display        Picasso
    /0/100/14.3                 bridge         FCH LPC Bridge
    /0/101                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/102                      bridge         Family 17h (Models 00h-1fh) PCIe Dumm
    /0/103                      bridge         Raven/Raven2 Device 24: Function 0
    /0/104                      bridge         Raven/Raven2 Device 24: Function 1
    /0/105                      bridge         Raven/Raven2 Device 24: Function 2
    /0/106                      bridge         Raven/Raven2 Device 24: Function 3
    /0/107                      bridge         Raven/Raven2 Device 24: Function 4
    /0/108                      bridge         Raven/Raven2 Device 24: Function 5
    /0/109                      bridge         Raven/Raven2 Device 24: Function 6
    /0/10a                      bridge         Raven/Raven2 Device 24: Function 7
    /1                  eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:01.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:01.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 61)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 0
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 1
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 2
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 3
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 4
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 5
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 6
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 7
    01:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8822CE 802.11ac PCIe Wireless Network Adapter
    02:00.0 SD Host controller: Genesys Logic, Inc GL9750 SD Host Controller (rev 01)
    03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso (rev ea)
    03:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
    03:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
    03:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven2 USB 3.1
    03:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor
    03:00.7 Non-VGA unclassified device: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/Renoir Sensor Fusion Hub
    ```

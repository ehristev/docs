---
title: Grunt Chromebooks
---

`grunt` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

-   [HP Chromebook 11 G6 EE](https://support.hp.com/in-en/product/hp-chromebook-11-g6-ee/18280450/document/c05904799)
-   [Acer Chromebook
    315](https://www.acer.com/ac/en/US/press/2019/479116)
-   [Acer Chromebook 311 (C721)](https://9to5google.com/2019/01/23/acer-amd-chromebooks-education/)

The specs for these chromebooks vary in terms of display, connectivity,
devices and CPU, some of them using Intel 64 bit processors such as the
Celeron N2940 (Bay Trail) and the Celeron N3350 (Apollo Lake) and some
others using AMD 64 bit Stoney Ridge processors like the AMD A4-9120.

The Collabora LAVA lab contains the following `grunt` devices:

-   [HP Chromebook 11 G6 EE](https://support.hp.com/in-en/product/hp-chromebook-11-g6-ee/18280450/document/c05904799) (codename `barla`)
    -   See [hp-11A-G6-EE-grunt](https://lava.collabora.dev/scheduler/device_type/hp-11A-G6-EE-grunt) in LAVA
    -   CPU: AMD A4-9120C
    -   GPU: Stoney [Radeon R2/R3/R4/R5 Graphics]
    -   Arch: x86_64
-   [HP Chromebook 14 db0003na](https://support.hp.com/ca-en/document/c06226085) (codename `careena`)
    -   See [hp-14-db0003na-grunt](https://lava.collabora.dev/scheduler/device_type/hp-14-db0003na-grunt) in LAVA
    -   CPU: AMD A4-9120C
    -   GPU: Stoney [Radeon R2/R3/R4/R5 Graphics]
    -   Arch: x86_64
-   [Acer Chromebook Spin 311 R721T](https://www.acer.com/ca-en/chromebooks/acer-chromebook-spin-311-cp311-3h/pdp/NX.HBRAA.002) (codename `kasumi360`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, KASUMI360, PVT, US, SKU3, AMD A4-9120C, 4GB RAM MICRON, 32GB HYNIX, TOUCH, 11.6IN, HD, WIFI, BT, USED`
    -   See [acer-R721T-grunt](https://lava.collabora.dev/scheduler/device_type/acer-R721T-grunt) in LAVA
    -   CPU: AMD A4-9120C
    -   GPU: Stoney [Radeon R2/R3/R4/R5 Graphics]
    -   Arch: x86_64

### Debugging interfaces

`grunt` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the HP 14-DB0003na, the debug port is the USB-C port in the right
side (also used for the power supply). In the HP Chromebook 11 G6 EE and Acer Chromebook Spin 311, the debug port is the USB-C port in the left side.

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

-   When using a Servo v4 as a debugging interface, the Cr50 interface
    shuts down after issuing a `cold_reset:on` command. However, this
    doesn't seem to happen if the Servo v4 is connected to a power
    supply and charging the Chromebook.

-   In some setups the serial console gets stuck and freezes during one
        of the Coreboot stages. Closing and reopening the serial terminal
        recovers the problem, since the Chromebook keeps running. Most of
        the times this happens is at this point during the boot process:

        POST: 0x40
        agesawrapper_amdinitpost() entry
        DRAM clear on reset: Keep
        variant_mainboard_read_spd SPD index 9
        CBFS: 'Master Header Locator' located CBFS at [df0000:ffffc0)
        CBFS: Locating 'spd.bin'
        CBFS: Found @ offset 79bc0 size 2000

    See also [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=192.168.201.1:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=192.168.201.1

the IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all `barla` units in the lab (based on the ChromeOS `grunt` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/grunt-barla.bin>

Firmware flashed on all `careena` units in the lab (based on the ChromeOS `grunt` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/grunt-careena.bin>

Firmware flashed on all `kasumi360` units in the lab (based on the ChromeOS `aleena` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/grunt-kasumi360.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `hp-11A-G6-EE-grunt`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path         Device  Class          Description
    ===================================================
    /0/0                     memory         1MiB BIOS
    /0/4                     processor      CPU [empty]
    /0/1                     memory         4GiB System memory
    /0/1/0                   memory         4GiB SODIMM DDR4 Synchronous 933 MHz (1.
    /0/2                     processor      AMD A4-9120C RADEON R4, 5 COMPUTE CORES
    /0/100                   bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/1                 display        Stoney [Radeon R2/R3/R4/R5 Graphics]
    /0/100/2.2               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/2.2/0             network        QCA6174 802.11ac Wireless Network Adapte
    /0/100/2.4               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/14.3              bridge         FCH LPC Bridge
    /0/101                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/102                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/103                   bridge         Carrizo Audio Dummy Host Bridge
    /0/104                   bridge         Stoney HT Configuration
    /0/105                   bridge         Stoney Address Maps
    /0/106                   bridge         Stoney DRAM Configuration
    /0/107                   bridge         Stoney Miscellaneous Configuration
    /0/108                   bridge         Stoney PM Configuration
    /0/109                   bridge         Stoney NB Performance Monitor
    /1               eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Complex
    00:01.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Stoney [Radeon R2/R3/R4/R5 Graphics] (rev eb)
    00:01.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Device 15b3
    00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:02.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:02.4 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:03.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:08.0 Encryption controller: Advanced Micro Devices, Inc. [AMD] Carrizo Platform Security Processor
    00:09.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Carrizo Audio Dummy Host Bridge
    00:10.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB XHCI Controller (rev 20)
    00:12.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB EHCI Controller (rev 49)
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 4b)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 11)
    00:14.7 SD Host controller: Advanced Micro Devices, Inc. [AMD] FCH SD Flash Controller (rev 01)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney HT Configuration
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Address Maps
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney DRAM Configuration
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Miscellaneous Configuration
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney PM Configuration
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney NB Performance Monitor
    01:00.0 Network controller: Qualcomm Atheros QCA6174 802.11ac Wireless Network Adapter (rev 32)
    02:00.0 SD Host controller: O2 Micro, Inc. Device 8620 (rev 01)
    ```

-   `hp-14-db0003na-grunt`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path         Device  Class          Description
    ===================================================
    /0/0                     memory         1MiB BIOS
    /0/4                     processor      CPU [empty]
    /0/1                     memory         4GiB System memory
    /0/1/0                   memory         4GiB SODIMM DDR4 Synchronous 933 MHz (1.
    /0/2                     processor      AMD A4-9120C RADEON R4, 5 COMPUTE CORES
    /0/100                   bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/1                 display        Stoney [Radeon R2/R3/R4/R5 Graphics]
    /0/100/2.2               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/2.2/0             network        QCA6174 802.11ac Wireless Network Adapte
    /0/100/2.4               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/14.3              bridge         FCH LPC Bridge
    /0/101                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/102                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/103                   bridge         Carrizo Audio Dummy Host Bridge
    /0/104                   bridge         Stoney HT Configuration
    /0/105                   bridge         Stoney Address Maps
    /0/106                   bridge         Stoney DRAM Configuration
    /0/107                   bridge         Stoney Miscellaneous Configuration
    /0/108                   bridge         Stoney PM Configuration
    /0/109                   bridge         Stoney NB Performance Monitor
    /1               eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Complex
    00:01.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Stoney [Radeon R2/R3/R4/R5 Graphics] (rev eb)
    00:01.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Device 15b3
    00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:02.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:02.4 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:03.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:08.0 Encryption controller: Advanced Micro Devices, Inc. [AMD] Carrizo Platform Security Processor
    00:09.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Carrizo Audio Dummy Host Bridge
    00:10.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB XHCI Controller (rev 20)
    00:12.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB EHCI Controller (rev 49)
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 4b)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 11)
    00:14.7 SD Host controller: Advanced Micro Devices, Inc. [AMD] FCH SD Flash Controller (rev 01)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney HT Configuration
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Address Maps
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney DRAM Configuration
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Miscellaneous Configuration
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney PM Configuration
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney NB Performance Monitor
    01:00.0 Network controller: Qualcomm Atheros QCA6174 802.11ac Wireless Network Adapter (rev 32)
    02:00.0 SD Host controller: O2 Micro, Inc. Device 8620 (rev 01)
    ```

-   `acer-R721T-grunt`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path         Device  Class          Description
    ===================================================
    /0/0                     memory         1MiB BIOS
    /0/4                     processor      CPU [empty]
    /0/1                     memory         4GiB System memory
    /0/1/0                   memory         4GiB SODIMM DDR4 Synchronous 933 MHz (1.
    /0/2                     processor      AMD A4-9120C RADEON R4, 5 COMPUTE CORES
    /0/100                   bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/1                 display        Stoney [Radeon R2/R3/R4/R5 Graphics]
    /0/100/2.2               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/2.2/0             network        QCA6174 802.11ac Wireless Network Adapte
    /0/100/2.4               bridge         Family 15h (Models 60h-6fh) Processor Ro
    /0/100/14.3              bridge         FCH LPC Bridge
    /0/101                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/102                   bridge         Family 15h (Models 60h-6fh) Host Bridge
    /0/103                   bridge         Carrizo Audio Dummy Host Bridge
    /0/104                   bridge         Stoney HT Configuration
    /0/105                   bridge         Stoney Address Maps
    /0/106                   bridge         Stoney DRAM Configuration
    /0/107                   bridge         Stoney Miscellaneous Configuration
    /0/108                   bridge         Stoney PM Configuration
    /0/109                   bridge         Stoney NB Performance Monitor
    /1               eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Complex
    00:01.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Stoney [Radeon R2/R3/R4/R5 Graphics] (rev eb)
    00:01.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Device 15b3
    00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:02.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:02.4 PCI bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Processor Root Port
    00:03.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 15h (Models 60h-6fh) Host Bridge
    00:08.0 Encryption controller: Advanced Micro Devices, Inc. [AMD] Carrizo Platform Security Processor
    00:09.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Carrizo Audio Dummy Host Bridge
    00:10.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB XHCI Controller (rev 20)
    00:12.0 USB controller: Advanced Micro Devices, Inc. [AMD] FCH USB EHCI Controller (rev 49)
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 4b)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 11)
    00:14.7 SD Host controller: Advanced Micro Devices, Inc. [AMD] FCH SD Flash Controller (rev 01)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney HT Configuration
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Address Maps
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney DRAM Configuration
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney Miscellaneous Configuration
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney PM Configuration
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Stoney NB Performance Monitor
    01:00.0 Network controller: Qualcomm Atheros QCA6174 802.11ac Wireless Network Adapter (rev 32)
    02:00.0 SD Host controller: O2 Micro, Inc. Device 8620 (rev 01)
    ```

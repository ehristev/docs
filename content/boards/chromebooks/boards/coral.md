---
title: Coral Chromebooks
---

`coral` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

-   [Lenovo 100e
    Chromebook](https://www.lenovo.com/us/en/laptops/lenovo/lenovo-n-series/100e-Chromebook/p/88ELC1S9999)
-   [Acer Chromebook Spin
    11](https://www.acer.com/ac/en/AU/press/2018/333192)
-   [ASUS Chromebook C523NA](https://www.asus.com/us/Laptops/ASUS-Chromebook-C523NA/)

The specs for these chromebooks vary in terms of display, connectivity
and devices, but they are based on Intel Apollo Lake 64 bit CPUs
(Celeron Dual-Core N3350 , N3450 or Quad-Core N4200).

The Collabora LAVA lab contains the following `coral` devices:

-   [ASUS Chromebook C523NA](https://www.asus.com/Commercial-Laptops/ASUS-Chromebook-C523NA/specifications/) (codename `babytiger`)
    -   See [asus-C523NA-A20057-coral](https://lava.collabora.dev/scheduler/device_type/asus-C523NA-A20057-coral) in LAVA
    -   CPU: Intel Pentium CPU N4200 @ 1.10GHz
    -   GPU: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series
    -   Arch: x86_64
### Debugging interfaces

`coral` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the Asus C523NA, the debug port is the USB-C port in the left side
(also used for the power supply).

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

#### Known issues

-   [`coral-1`](https://lava.collabora.dev/scheduler/device/asus-C523NA-A20057-coral-cbg-1)
    fails to boot in around 5% of the attempts. One of the Coreboot
    stages resets the board and then it never comes out of reset (last
    console message: `CSE timed out. Resetting`).

See also [Common issues](../common_issues.md).

### Example kernel command line arguments

    earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `coral` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/coral.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `asus-C523NA-A20057-coral`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      CPU [empty]
    /0/1                   memory         4GiB System memory
    /0/1/0                 memory         1GiB Synchronous 2400 MHz (0.4 ns)
    /0/1/1                 memory         1GiB Synchronous 2400 MHz (0.4 ns)
    /0/1/2                 memory         1GiB Synchronous 2400 MHz (0.4 ns)
    /0/1/3                 memory         1GiB Synchronous 2400 MHz (0.4 ns)
    /0/2                   processor      Intel(R) Pentium(R) CPU N4200 @ 1.10GHz
    /0/100                 bridge         Celeron N3350/Pentium N4200/Atom E3900 Ser
    /0/100/2               display        Celeron N3350/Pentium N4200/Atom E3900 Ser
    /0/100/14              bridge         Celeron N3350/Pentium N4200/Atom E3900 Ser
    /0/100/14/0            network        Wireless 7265
    /0/100/1f              bridge         Celeron N3350/Pentium N4200/Atom E3900 Ser
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Host Bridge (rev 0b)
    00:00.1 Signal processing controller: Intel Corporation Device 5a8c (rev 0b)
    00:00.2 Non-Essential Instrumentation [1300]: Intel Corporation Device 5a8e (rev 0b)
    00:02.0 VGA compatible controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Integrated Graphics Controller (rev 0b)
    00:03.0 Multimedia controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Imaging Unit (rev 0b)
    00:0e.0 Multimedia audio controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Audio Cluster (rev 0b)
    00:0f.0 Communication controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Trusted Execution Engine (rev 0b)
    00:14.0 PCI bridge: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series PCI Express Port B #1 (rev fb)
    00:15.0 USB controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series USB xHCI (rev 0b)
    00:16.0 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #1 (rev 0b)
    00:16.1 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #2 (rev 0b)
    00:16.2 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #3 (rev 0b)
    00:16.3 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #4 (rev 0b)
    00:17.0 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #5 (rev 0b)
    00:17.1 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series I2C Controller #6 (rev 0b)
    00:18.0 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series HSUART Controller #1 (rev 0b)
    00:18.1 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series HSUART Controller #2 (rev 0b)
    00:18.2 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series HSUART Controller #3 (rev 0b)
    00:19.0 Signal processing controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series SPI Controller #1 (rev 0b)
    00:1b.0 SD Host controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series SDXC/MMC Host Controller (rev 0b)
    00:1c.0 SD Host controller: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series eMMC Controller (rev 0b)
    00:1f.0 ISA bridge: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series Low Pin Count Interface (rev 0b)
    00:1f.1 SMBus: Intel Corporation Celeron N3350/Pentium N4200/Atom E3900 Series SMBus Controller (rev 0b)
    01:00.0 Network controller: Intel Corporation Wireless 7265 (rev 59)
    ```

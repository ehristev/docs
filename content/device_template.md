
---
title: Template device
---

This document is a template for document a device type to be added to Lava.
It is intended to be used by the Lava lab team, so please make sure it does not
assume prior knowledge with a given board, bootloader, SOC and any related
tooling.

This section provided the introduction for the device device for example:

<Insert pretty picture here>

This is a badger board build by $vendor

specification:
* Quad Badger CPU at mushroom hz
* 2 mushrooms of memory
* Mali Bitfrost GPU
* 4 Snake ports
* <other specs here>

This board is available from the [badger store](https://www.youtube.com/watch?v=EIyixC9NsLI).
For use in lava the magical trevor debug board is also required from
[the store](https://www.youtube.com/watch?v=au3-hk-pXsM&list=PL03A92E8F025C358A)

### Power control

This section should document how power/reset is controlled for a board; For a lot of
boards this is simply by switching the power on their power plug:

* 5V/4A DC input, 2.1mm x 5.5mm, center positive

Some other boards; use standard USB connectors for power *without* connecting
data which can be easily supported:

* 5V/4A USB-A connector; Data lines not connected

Things do get more complex if the data lines *are* connected and they're needed
for functionality; In that case a device like the
[YKUSH XS](https://www.yepkit.com/product/300115/YKUSHXS) is worth considering.
However keep in mind that adds  power draw to the USB hub used. So if a device
can be powered directly from e.g. 5v (via pins) that's preferable.

For newer devices using USB-C as input we don't have a standard solution yet.

For devices like chromebooks with a dedicated debug board (e.g. servo) document
both the normal power and reset. Ditto for devices which need to be reset via
pins. e.g:
* 12V/2A power supply via standard adapter or DC jack
* Reset by pulling pin J1 to ground for 3 seconds.

For these more complex cases please also include either pictures or diagrams
showing the pins etc.

### Low-level boot control

Most developer devices luckily have some kind of serial port; Typically at TTL
level exposed via standard gpio pins. For those document the pin
location/layout (with photo and/or diagrams) as well as the basic
specifications:
* 1.8V TTL level standard serial port (gnd, tx, rx)
* baudrate 115200 8n1

If the device has a dedicated debug board with specialised tooling please
document that in detail.

#### Network connectivity

Network connectivity is typically needed during early boot (from the
bootloader) and for some test jobs (e.g. using NFS root). So whatever network
connection option is document here it also needs to be supported by at least
the bootloader.

For most devices this will be a standard Ethernet port which can simply be
noted. If the devices cannot auto-negotiate for some reason (or has issues at
higher rates e.g. gigabit) please document that as well.

In case devices rely on peripherals for networking (e.g. USB dongles) please
document which one has been tested with the devices (and supported by the
bootloader).

For really minimal devices the DUT might only be able to expose itself as a
network interface over USB OTG. Which can be supported but keep in mind that
is not a common setup (and again has to be supported by the bootloader as
well). For this setup ensure to document which USB devices/profiles are used
during the various stages.

### Bootloader

This section is the first step about actual software :). It should document
where to get the bootloader and in detail how to flash/configure it.

It should also do a base document of where the bootloader is stored on the
actual device (e.g. SD card, boot sector of builtin storage, dedicated flash
area). This can help in both troubleshooting and defining which tests can be
run (e.g. one cannot use the SD card for testing if the bootloader is stored there)

If possible configure the device to retrieve the bootloader from non-mass
storage (e.g. network, SPI flash, eMMC hardware boot partition) for maximum
flexibility and reliability.

Any device specific boot commands should also be documented here; E.g. for
u-boot the load address for kernel, initrd and dtb should be documented.


Please note that all the information should be available for as long as the
devices should be supported by the lab. Often this is longer then the interest
of the vendor. So only refer to external documentation if this is expected to
be available for a long time.

For artifacts to be flashed or provided over the network, please store these in
a stable location (e.g. gitlab repository) as well. For artifacts that can be
build from source, please include documention on how to do that, ideally in the
form of a git repository with a gitlab-ci job to run the build.

### Health checks

For a basic health check job typically all that is needed is a working kernel
build. Ideally boards (and their network interfaces) are supported upstream
already using the platforms defconfig; In which case this section can just
mention that e.g.:
* Supported upstream by defconfig from Linux 4.19 and onwards

For devices that are not yet upstream please provide documentation on how a
supported kernel that can be used for health checks. Same considerations as the
bootloader apply; This should, if possible, be in the form of a git repository
on gitlab with a gitlab ci job to build the artifacts (kernel image, dtb(s),
modules) for reproducibility.

If a device needs a more complex health check e.g. adding firmware, doing
specific operations/checks, please discuss with the lab team.

### Lab notes and trouble shooting

This section is for extra notes from the lab team while working with the device
and common issues that might occur.

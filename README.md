Repo for lava documentation. The canonical copy can be found on https://lava.pages.collabora.com/docs/

## Contributing

Patches welcome! Please submit MRs against the gitlab repository


## Building/testing locally

To work locally with this project simply run the `run-hugo.sh` script; This
will spawn a hugo (in server mode) in a docker container allowing to use your
browser for previes.  Hugo watches the repository directory and automatically
rerenders on changes.

Read more at Hugo's [documentation][].

### GitLab CI

The content of this project is built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[documentation]: https://gohugo.io/overview/introduction/

#!/bin/sh

# Run hugo in server mode and let it bind on all interface in the container
# such that docker can forward it properly.

REPO=$(dirname $(realpath $0))

docker run --rm \
  -p 1313:1313/tcp \
  -w /docs \
  -u $(id -u) \
  -v ${REPO}:/docs \
  registry.gitlab.com/pages/hugo:latest \
  hugo server --bind ""
